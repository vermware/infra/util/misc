#!/bin/sh

set -eu

build="$1"

if [ -f "$build/ping" ]; then
	hash ping || apt-get -qy install iputils-ping
	cp -p -- "$build/ping" /etc/cron.hourly
fi
