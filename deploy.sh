#!/bin/sh

set -eu

hash rsync
hash ssh
hash ssh-agent

host="$1"

if [ "${SSH_KEY:-}" ]; then
	eval "$(ssh-agent)"
	<<-EOF ssh-add -q -
		$SSH_KEY
	EOF
fi
if [ "${SSH_KNOWNHOSTS:-}" ]; then
	# Sanity check, do not overwrite existing.
	[ -e ~/.ssh/known_hosts ] && exit 1
	mkdir -p -- ~/.ssh
	<<-EOF cat >~/.ssh/known_hosts
		$SSH_KNOWNHOSTS
	EOF
fi

ssh "root@$host" "sh -c -- 'hash rsync || apt-get install -qy rsync'"
rsync -rlpt --delete -- build/ "root@$host:/tmp/misc_deploy_tmp"
ssh "root@$host" 'sh -s -- /tmp/misc_deploy_tmp' <install.sh
ssh "root@$host" 'rm -r -- /tmp/misc_deploy_tmp'

if [ "${SSH_KEY:-}" ]; then
	eval "$(ssh-agent -k)"
fi
if [ "${SSH_KNOWNHOSTS:-}" ]; then
	rm -- ~/.ssh/known_hosts
fi
