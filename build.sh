#!/bin/sh

set -eu
umask 0077

host="$1"
. "host/$host/config.sh"

[ -d build ] && rm -r -- build
mkdir -- build

for i in $components; do
	case "$i" in
	ping)
		cp -- "src/$i" build
		chmod -- 0700 "build/$i"
		;;
	*)
		printf 'error: unhandled component %s\n' "$i" >&2
		;;
	esac
done
